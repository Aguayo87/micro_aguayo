import { Schema, model } from 'mongoose';

const userSchema = new Schema ( {
    Nombre: { type: String, required: true},
    Apellido: { type: String, required: true},
    Edad: Number,
    FecNacimiento: { type: Date, default:Date.now }
    
});

export default model('User', userSchema);
