import {Request, Response, Router, json } from 'express';

import User from '../models/User';

class userRoutes{

    router: Router

    constructor() {
        this.router = Router();
        this.routes();

    }
//Crear usuario
    public async createUser(req: Request, res: Response): Promise <void> {
        const { Nombre, Apellido, Edad, FecNacimiento} = req.body;
        const newUser = new User ({ Nombre, Apellido, Edad, FecNacimiento });
        await newUser.save();
        res.json({data: newUser});
    }
//Buscar todo
    public async getUser(req: Request, res: Response): Promise <void> {
        const user = await User.findOne({Nombre: req.params.Nombre});
        res.json(user)
    }
//Bucar un usuario
    public async getUsers(req: Request, res: Response): Promise <void> {
        const users = await User.find();
        res.json(users);
    }
//Modificar
    public async updateUser(req: Request, res: Response): Promise <void> {
        const { Nombre } = req.params;
        const user = await User.findOneAndUpdate({Nombre}, req.body, {new: true})
        res.json(user)
    }
//Borrar
    public async deleteUser(req: Request, res: Response): Promise <void> {
        const { Nombre } = req.params;
        await User.findOneAndDelete({Nombre});
        res.json({Response: 'Usuario eliminado satisfactoriamente.'});
    }

    routes() {
        this.router.post('/', this.createUser);
        this.router.get('/:Nombre', this.getUser);
        this.router.get('/', this.getUsers);   
        this.router.put('/:Nombre', this.updateUser);
        this.router.delete('/:Nombre', this.deleteUser);
        

    }
}

const UserRoutes = new userRoutes();
export default UserRoutes.router;