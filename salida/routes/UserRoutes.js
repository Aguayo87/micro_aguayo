"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const User_1 = __importDefault(require("../models/User"));
class userRoutes {
    constructor() {
        this.router = express_1.Router();
        this.routes();
    }
    //Crear usuario
    createUser(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { Nombre, Apellido, Edad, FecNacimiento } = req.body;
            const newUser = new User_1.default({ Nombre, Apellido, Edad, FecNacimiento });
            yield newUser.save();
            res.json({ data: newUser });
        });
    }
    //Buscar todo
    getUser(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const user = yield User_1.default.findOne({ Nombre: req.params.Nombre });
            res.json(user);
        });
    }
    //Bucar un usuario
    getUsers(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const users = yield User_1.default.find();
            res.json(users);
        });
    }
    //Modificar
    updateUser(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { Nombre } = req.params;
            const user = yield User_1.default.findOneAndUpdate({ Nombre }, req.body, { new: true });
            res.json(user);
        });
    }
    //Borrar
    deleteUser(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { Nombre } = req.params;
            yield User_1.default.findOneAndDelete({ Nombre });
            res.json({ Response: 'Usuario eliminado satisfactoriamente.' });
        });
    }
    routes() {
        this.router.post('/', this.createUser);
        this.router.get('/:Nombre', this.getUser);
        this.router.get('/', this.getUsers);
        this.router.put('/:Nombre', this.updateUser);
        this.router.delete('/:Nombre', this.deleteUser);
    }
}
const UserRoutes = new userRoutes();
exports.default = UserRoutes.router;
