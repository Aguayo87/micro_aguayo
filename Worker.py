import os
from MsCoppel import Microservices, Options, Types, MsManager, ErrorMs


@MsManager.Define(
    Options(
        'PracticaMicroservicio',
        'MicroservicioAguayo',
        'v1',
        os.environ.get('NATS', '127.0.0.1:4222').split(','),
        # En caso de kafka eliminar linea
        is_nats=False if os.environ.get('NATS', None) is None else True,
    )
)
class MicroservicioAguayo(Microservices):

    def smoketest(self):
        """
            Metodo que se utiliaza para validar el servicio
            desde una consulta REST.

            su valor de retorno siempre es logico.
        """
        True

    @MsManager.Errors
    def misErrores(self):
        return {'-12': 'Error definido por el usuario'}

    @MsManager.Listener(ctx=['data'])
    def lister(self, data):
        raise ErrorMs(-12)
