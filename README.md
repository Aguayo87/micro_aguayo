Microservicio Aguayo - Local

POSTMAN

//Create
POST	http://localhost:3000/api/users

{
	"Nombre" : "Dylan", 
	"Apellido" : "Aguayo", 
	"Edad" : 19 ,
	"FecNacimiento" : "2001-04-23"
}

-----------------------------------------------------------------

//Consultar todo
GET	http://localhost:3000/api/users

-----------------------------------------------------------------

//Consultar un usuario
GET	http://localhost:3000/api/users/Dylan

-----------------------------------------------------------------

//Modificar
UPDATE	http://localhost:3000/api/users/Dylan

{
	"Nombre" : "Dylan", 
	"Apellido" : "Flores", 
	"Edad" : 20 ,
	"FecNacimiento" : "2000-04-23"
}

-------------------------------------------------------------------

//Eliminar

DELETE	http://localhost:3000/api/users/Dylan